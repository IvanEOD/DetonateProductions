package com.detonateproductions.api.utility

import kotlin.math.ceil
import kotlin.math.floor
import kotlin.math.roundToInt


/* Written by IvanEOD 1/23/2023, at 10:50 AM */

fun lowestAbove(min: Number, vararg others: Number): Number? {
    var lowest: Number? = null
    for (i in others) {
        if (i > min) {
            if (lowest == null || lowest < i) lowest = i
        }
    }
    return lowest
}

operator fun Number.compareTo(other: Number) = when {
    this.toDouble() > other.toDouble() -> 1
    this.toDouble() < other.toDouble() -> -1
    else -> 0
}

fun Iterable<Number>.lowestAbove(min: Number): Number? = lowestAbove(min, *this.toList().toTypedArray())

const val OsrsMax = 2147483647
const val OneSecond = 1000
const val OneMinute = OneSecond * 60
const val OneHour = OneMinute * 60
const val OneDay = OneHour * 24

fun Number.isEven() = this.toInt() % 2 == 0
fun Number.isOdd() = !isEven()

fun Long.toSeconds() = this / 1000
fun Long.toMinutes() = this / 1000 / 60

fun Number.round(step: Number): Number = (toDouble() / step.toDouble()).roundToInt() * step.toDouble()

fun Number.getPercentage(zeroToOneHundred: Number) = (toDouble() / 100.0) * zeroToOneHundred.toDouble()
fun Number.getPercentage(zeroToOneHundred: Number, roundStep: Number) = getPercentage(zeroToOneHundred).round(roundStep)

fun calculateChance(percentPerRoll: Number, rolls: Number) = minOf(100.0, percentPerRoll.toDouble() * rolls.toDouble())

fun Number.roundDownToNearest(step: Number): Double = (floor(this.toDouble() / step.toDouble()) * step.toDouble())
fun Number.roundUpToNearest(step: Number): Double = (ceil(this.toDouble() / step.toDouble()) * step.toDouble())

fun lerp(start: Number, finish: Number, progress: Number): Double =
    start.toDouble() + (finish.toDouble() - start.toDouble()) * progress.toDouble()


fun String.convertToInteger(): Int {
    var intString = this
    if (intString.equals("all", true)) return OsrsMax
    intString = when (intString.last()) {
        'k' -> intString.substring(0, intString.length - 1) + "000"
        'm' -> intString.substring(0, intString.length - 1) + "000000"
        'b' -> intString.substring(0, intString.length - 1) + "000000000"
        else -> intString
    }
    return intString.toInt()
}

private val ThousandsRegex = "^\\d+(000)$".toRegex()
private val MillionsRegex = "^\\d+(000000)$".toRegex()
private val BillionsRegex = "^\\d+(000000000)$".toRegex()
private val RoundingRegexes = listOf(
    "^\\d+(000)$".toRegex(),
    "^\\d+(000000)$".toRegex(),
    "^\\d+(000000000)$".toRegex()
)

fun Number.endsInThousandMillionOrBillionZeroes() = toInt().toString().endsInThousandMillionOrBillionZeroes()
fun String.endsInThousandMillionOrBillionZeroes(): Boolean = RoundingRegexes.any { this.hasMatches(it) }

fun String.abbreviateNumber(): String {
    if (!endsInThousandMillionOrBillionZeroes()) return this
    return if (hasMatches(BillionsRegex)) replaceLastMatchGroup(BillionsRegex, "b")
    else if (hasMatches(MillionsRegex)) replaceLastMatchGroup(MillionsRegex, "m")
    else if (hasMatches(ThousandsRegex)) replaceLastMatchGroup(ThousandsRegex, "k")
    else this
}

fun Int.toAbbreviatedString(): String = getRoundedAmount().toString().abbreviateNumber()

fun Int.getRoundedAmount(): Int {
    val roundUpToNearest: Int = when {
        this >= 1000000000 -> 1000000
        this >= 100000000 -> 500000
        this >= 10000000 -> 100000
        this >= 1000000 -> 10000
        this >= 100000 -> 1000
        this >= 10000 -> 500
        this >= 1000 -> 100
        else -> 5
    }
    return round(roundUpToNearest).toInt()
}