package com.detonateproductions.api.utility


/* Written by IvanEOD 1/23/2023, at 10:30 AM */

fun String.hasMatches(regex: Regex) = regex.containsMatchIn(this)
fun String.hasMatches(regex: String) = regex.toRegex().containsMatchIn(this)
fun String.lastMatch(regex: Regex): Pair<String, Int> {
    var lastMatch = ""
    for (match in regex.findAll(this)) lastMatch = match.value
    return lastMatch to this.lastIndexOf(lastMatch)
}
fun String.lastMatch(regex: String): Pair<String, Int> = lastMatch(regex.toRegex())
fun String.replaceLast(other: String, replacement: String): String {
    val index = this.lastIndexOf(other)
    return if (index == -1) this else this.substring(0, index) + replacement + this.substring(index + other.length)
}
fun String.replaceLast(regex: Regex, replacement: String) = apply {
    val (last, index) = lastMatch(regex)
    if (last.isEmpty() || index == -1) return this
    replaceRange(index, index + last.length, replacement)
}
fun String.getAllMatchGroups(regex: Regex) = regex.findAll(this).map { it.groupValues }.toList().subList(1, this.length)
fun String.getAllMatchGroups(regex: String) = getAllMatchGroups(regex.toRegex())
fun String.getAllCompleteMatches(regex: Regex) = regex.findAll(this).map { it.value }.toList()
fun String.getAllCompleteMatches(regex: String) = getAllCompleteMatches(regex.toRegex())
fun String.replaceAllMatchGroups(regex: Regex, replacement: String) =
    getAllMatchGroups(regex).fold(this) { acc, list -> acc.replace(list[0], replacement) }
fun String.replaceAllMatchGroups(regex: String, replacement: String) =
    replaceAllMatchGroups(regex.toRegex(), replacement)
fun String.replaceAllCompleteMatches(regex: Regex, replacement: String) =
    getAllCompleteMatches(regex).fold(this) { acc, match -> acc.replace(match, replacement) }
fun String.replaceAllCompleteMatches(regex: String, replacement: String) =
    replaceAllCompleteMatches(regex.toRegex(), replacement)
fun String.getFirstMatchGroup(regex: Regex) = regex.find(this)?.groupValues?.getOrNull(1) ?: ""
fun String.getFirstMatchGroup(regex: String) = getFirstMatchGroup(regex.toRegex())
fun String.getFirstCompleteMatch(regex: Regex) = regex.find(this)?.value ?: ""
fun String.getFirstCompleteMatch(regex: String) = getFirstCompleteMatch(regex.toRegex())
fun String.replaceFirstMatchGroup(regex: Regex, replacement: String) =
    getFirstMatchGroup(regex).let { if (it.isEmpty()) this else replaceFirst(it, replacement) }
fun String.replaceFirstMatchGroup(regex: String, replacement: String) =
    replaceFirstMatchGroup(regex.toRegex(), replacement)
fun String.replaceFirstCompleteMatch(regex: Regex, replacement: String) =
    getFirstCompleteMatch(regex).let { if (it.isEmpty()) this else replaceFirst(it, replacement) }
fun String.replaceFirstCompleteMatch(regex: String, replacement: String) =
    replaceFirstCompleteMatch(regex.toRegex(), replacement)
fun String.getLastMatchGroup(regex: Regex) =
    regex.findAll(this).map { it.groupValues }.toList().lastOrNull()?.getOrNull(1) ?: ""
fun String.getLastMatchGroup(regex: String) = getLastMatchGroup(regex.toRegex())
fun String.getLastCompleteMatch(regex: Regex) = regex.findAll(this).map { it.value }.toList().lastOrNull() ?: ""
fun String.getLastCompleteMatch(regex: String) = getLastCompleteMatch(regex.toRegex())
fun String.replaceLastMatchGroup(regex: Regex, replacement: String): String {
    val lastMatchGroup = getLastMatchGroup(regex)
    if (lastMatchGroup.isEmpty()) return this
    return replaceLast(lastMatchGroup, replacement)
}
fun String.replaceLastMatchGroup(regex: String, replacement: String) =
    replaceLastMatchGroup(regex.toRegex(), replacement)
fun String.replaceLastCompleteMatch(regex: Regex, replacement: String) =
    getLastCompleteMatch(regex).let { if (it.isEmpty()) this else replaceLast(it, replacement) }
fun String.replaceLastCompleteMatch(regex: String, replacement: String) =
    replaceLastCompleteMatch(regex.toRegex(), replacement)

fun String.toIntArray() = this.split(",").mapNotNull { it.trim().toIntOrNull() }.toIntArray()
