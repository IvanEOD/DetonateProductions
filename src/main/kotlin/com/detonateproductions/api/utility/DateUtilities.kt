package com.detonateproductions.api.utility

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.temporal.ChronoUnit


/* Written by IvanEOD 1/23/2023, at 11:05 AM */

fun Long.toLocalDateTime(): LocalDateTime = Instant.ofEpochMilli(this).atZone(ZoneId.systemDefault()).toLocalDateTime()
fun LocalDateTime.toMilliseconds(): Long = atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()

fun LocalDateTime.durationFrom(other: LocalDateTime, unit: ChronoUnit): Long = unit.between(other, this)
fun LocalDateTime.durationFromNow(unit: ChronoUnit): Long = durationFrom(LocalDateTime.now(), unit)

fun LocalDateTime.millisecondsFrom(other: LocalDateTime): Long = durationFrom(other, ChronoUnit.MILLIS)
fun LocalDateTime.millisecondsFromNow(): Long = durationFromNow(ChronoUnit.MILLIS)

fun dateFromNow(duration: Long, unit: ChronoUnit): LocalDateTime = LocalDateTime.now().plus(duration, unit)


