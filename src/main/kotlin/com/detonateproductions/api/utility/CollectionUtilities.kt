package com.detonateproductions.api.utility

import java.util.*


/* Written by IvanEOD 1/23/2023, at 10:58 AM */


fun joinIntArrays(vararg arrays: IntArray): IntArray {
    var result = intArrayOf()
    for (array in arrays) {
        result = result.copyOf(result.size + array.size)
        System.arraycopy(array, 0, result, result.size - array.size, array.size)
    }
    return result
}
inline fun <reified T : Any> joinArrays(vararg arrays: Array<T>) : Array<T> {
    var result: Array<T?>? = null
    for (array in arrays) {
        result = result?.copyOf(result.size + array.size) ?: array.copyOf(array.size)
        System.arraycopy(array, 0, result, result.size - array.size, array.size)
    }
    return result?.filterNotNull()?.toTypedArray() ?: arrayOf()
}
