package com.detonateproductions.api

import java.awt.Image
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL
import java.nio.file.Files
import java.nio.file.Path
import java.util.*
import javax.imageio.ImageIO
import kotlin.collections.HashMap

class GitHub(
    directory: Directory,
    val baseUrl: String,
    private val token: String = ""
) : Directory by directory {

    constructor(path: Path, baseUrl: String, token: String = "") : this(Directory.from(path), baseUrl, token)



    private val authString: String? = if (token.isEmpty()) null else "Basic " + Base64.getEncoder()
        .encodeToString("$token:x-oauth-basic".toByteArray())

    private var _downloadCache: GithubDownloadCache? = null
    private var downloadCache: GithubDownloadCache
        get() {
            if (_downloadCache == null) {
                _downloadCache = load(GithubDownloadCache::class.java, "cache") ?: GithubDownloadCache()
            }
            return _downloadCache!!
        }
        set(value) {}

    fun getImage(imageName: String): Image? = this.loadGitImage(imageName)
    fun getImageFile(imageName: String): File = getGitFile("images", "$imageName.png")
    fun getItemIconFile(itemId: Int): File = getGitFile("images/items-icons", "$itemId.png")
    fun getFont(fontName: String): File = getGitFile("fonts", "$fontName.ttf")
    fun getJson(jsonName: String): File = getGitFile("json", "$jsonName.json")
    fun getFxml(fxmlName: String): File = getGitFile("fxml", "$fxmlName.fxml")
    fun getCss(cssName: String): File = getGitFile("css", "$cssName.css")

    private fun getGitFile(folder: String, fileName: String): File {
        val currentEtag = downloadCache[folder, fileName]
        val checkedTag = getETag(folder, fileName)
        val path = getPath(folder, fileName)
        if (currentEtag != checkedTag) {
            if (Files.exists(path) && path.toFile().exists()) Files.delete(path)
        }
        if (!Files.exists(path) || !path.toFile().exists()) {
            downloadCache[folder, fileName] = checkedTag
            downloadGitFile(path.toFile(), folder, fileName)
        }
        return path.toFile()
    }

    fun getETag(folder: String, fileName: String): String {
        var etag = ""
        val urlString = "$baseUrl$folder/${fileName.replace(" ", "%20")}"
        try {
            val url = URL(urlString)
            val connection = url.openConnection()
            connection.setRequestProperty("Authorization", authString)
            etag = connection.getHeaderField("ETag") ?: ""
        } catch (ioException: IOException) {
            println("Error checking file from GitHub ${ioException.message}")
        }
        return etag.replace("\"".toRegex(), "")
    }

    private fun loadGitImage(imageName: String): Image? {
        val file = getGitFile("images", imageName)
        return if (file.exists()) {
            try {
                ImageIO.read(file)
            } catch (exception: IOException) {
                println("Error loading image from GitHub. ${exception.message}")
                null
            }
        } else null
    }

    private fun downloadGitFile(file: File, folderName: String, fileName: String) {
        val urlString = "$baseUrl$folderName/${fileName.replace(" ", "%20")}"
        try {
            val url = URL(urlString)
            val connection = url.openConnection()
            if (!file.exists() && !Files.exists(file.toPath())) file.createNewFile()
            if (authString != null) connection.setRequestProperty("Authorization", authString)
            val etag = connection.getHeaderField("ETag")
            etag?.replace("\"".toRegex(), "")
            val inStream = connection.getInputStream()
            val output = FileOutputStream(file)
            val buffer = ByteArray(4096)
            var index = 0
            while (-1 != inStream.read(buffer).also { index = it }) output.write(buffer, 0, index)
            inStream.close()
            output.close()
            downloadCache[folderName, fileName] = etag
            save(downloadCache, "cache")
        } catch (ioException: IOException) {
            println("Error downloading file from GitHub. ${ioException.message}")
        }
    }


    internal class GithubDownloadCache {
        private val cache = HashMap<String, HashMap<String, String>>()
        operator fun get(folder: String): HashMap<String, String> = cache.getOrPut(folder) { HashMap() }
        operator fun get(folder: String, fileName: String) = this[folder][fileName] ?: ""
        operator fun set(folder: String, fileName: String, etag: String) {
            this[folder][fileName] = etag
        }

        fun etagExists(folder: String, fileName: String) = this[folder].containsKey(fileName)
    }


}