package com.detonateproductions.api.flowbus


/* Written by IvanEOD 1/23/2023, at 11:57 AM */
inline fun <reified T: Any> FlowBus.dropEvent() = dropEvent(T::class.java)
inline fun <reified T : Any> FlowBus.getLastEvent(): T? = getLastEvent(T::class.java)
inline fun <reified T : Any> FlowBus.getFlow() = getFlow(T::class.java)
inline fun <reified T : Any> EventsReceiver.subscribe(skipRetained: Boolean = false, noinline callback: suspend (event: T) -> Unit): EventsReceiver =
    subscribeTo(T::class.java, skipRetained, callback)
inline fun <reified T : Any> EventsReceiver.subscribe(callback: EventCallback<T>, skipRetained: Boolean = false): EventsReceiver =
    subscribeTo(T::class.java, callback, skipRetained)