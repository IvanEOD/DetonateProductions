package com.detonateproductions.api.flowbus

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers


/* Written by IvanEOD 1/23/2023, at 11:57 AM */

interface FlowBusDispatchProvider {
    fun getDispatcher(): CoroutineDispatcher
}

internal class DefaultFlowBusDispatchProvider: FlowBusDispatchProvider {
    override fun getDispatcher() = Dispatchers.Default
}

internal object FlowBusDispatcherProvider {
    private const val DEFAULT_DISPATCHER_PROVIDER = "com.detonateproductions.api.flowbus.DefaultFlowBusDispatchProvider"

    private val provider by lazy {
        val providerNames = listOf(DEFAULT_DISPATCHER_PROVIDER)
        providerNames.firstNotNullOfOrNull { forName(it) } as FlowBusDispatchProvider? ?: DefaultFlowBusDispatchProvider()
    }

    private fun forName(name: String) = when (name) {
        DEFAULT_DISPATCHER_PROVIDER -> DefaultFlowBusDispatchProvider()
        else -> null
    }

    fun get() = provider.getDispatcher()

}