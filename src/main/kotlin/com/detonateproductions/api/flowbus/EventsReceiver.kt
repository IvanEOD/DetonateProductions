package com.detonateproductions.api.flowbus

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.filterNotNull


/* Written by IvanEOD 1/23/2023, at 11:57 AM */
open class EventsReceiver @JvmOverloads constructor(
    private val bus: FlowBus = GlobalBus
) {

    private val jobs = mutableMapOf<Class<*>, Job>()

    private var returnDispatcher: CoroutineDispatcher = FlowBusDispatcherProvider.get()

    fun returnOn(dispatcher: CoroutineDispatcher): EventsReceiver {
        returnDispatcher = dispatcher
        return this
    }

    @JvmOverloads
    fun <T : Any> subscribeTo(
        clazz: Class<T>,
        skipRetained: Boolean = false,
        callback: suspend (event: T) -> Unit
    ): EventsReceiver {
        if (jobs.containsKey(clazz)) throw IllegalStateException("Already subscribed for event type: ${clazz.simpleName}")
        val exceptionHandler = CoroutineExceptionHandler { _, throwable -> throw throwable }
        val job = CoroutineScope(Job() + Dispatchers.Default + exceptionHandler).launch {
            bus.forEvent(clazz)
                .drop(if (skipRetained) 1 else 0)
                .filterNotNull()
                .collect { withContext(returnDispatcher) { callback(it) } }
        }
        jobs[clazz] = job
        return this
    }

    @JvmOverloads
    fun <T: Any> subscribeTo(
        clazz: Class<T>,
        callback: EventCallback<T>,
        skipRetained: Boolean = false
    ): EventsReceiver = subscribeTo(clazz, skipRetained) { callback.onEvent(it) }

    fun <T: Any> unsubscribe(clazz: Class<T>) {
        jobs.remove(clazz)?.cancel()
    }

    fun unsubscribe() {
        jobs.values.forEach { it.cancel() }
        jobs.clear()
    }

}