package com.detonateproductions.api.flowbus

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch


/* Written by IvanEOD 1/23/2023, at 11:56 AM */
open class FlowBus {

    private val flows = mutableMapOf<Class<*>, MutableStateFlow<*>>()

    internal fun <T : Any> forEvent(clazz: Class<T>): MutableStateFlow<T?> =
        flows.getOrPut(clazz) { MutableStateFlow<T?>(null) } as MutableStateFlow<T?>

    fun <T : Any> getFlow(clazz: Class<T>): Flow<T> = forEvent(clazz).asStateFlow().filterNotNull()

    @JvmOverloads
    fun <T : Any> post(event: T, retain: Boolean = true) {
        val flow = forEvent(event.javaClass)
        flow.tryEmit(event).also {
            if (!it) throw IllegalStateException("StateFlow cannot take element, this should never happen")
        }
        if (!retain) {
            CoroutineScope(Job() + Dispatchers.Unconfined).launch {
                dropEvent(event.javaClass)
            }
        }
    }

    fun <T : Any> getLastEvent(clazz: Class<T>): T? = flows.getOrElse(clazz) { null }?.value as T?

    fun <T> dropEvent(clazz: Class<T>) {
        if (!flows.contains(clazz)) return
        val channel = flows[clazz] as MutableStateFlow<T?>
        channel.tryEmit(null)
    }

    fun dropAll() {
        flows.values.forEach { (it as MutableStateFlow<Any?>).tryEmit(null) }
    }


}