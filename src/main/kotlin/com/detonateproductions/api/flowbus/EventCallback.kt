package com.detonateproductions.api.flowbus


/* Written by IvanEOD 1/23/2023, at 11:57 AM */

fun interface EventCallback<T> {
    fun onEvent(event: T)
}