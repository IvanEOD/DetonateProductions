package com.detonateproductions.api

import com.detonateproductions.api.FileUtilities.Companion.createDirectories
import com.detonateproductions.api.FileUtilities.Companion.createSubDirectories
import com.detonateproductions.api.FileUtilities.Companion.deleteFiles
import com.detonateproductions.api.FileUtilities.Companion.deleteFilesOlderThan
import com.detonateproductions.api.FileUtilities.Companion.writeFile
import com.detonateproductions.api.Global.GSON
import com.google.gson.Gson
import java.nio.file.Path
import java.nio.file.Paths
import java.util.concurrent.TimeUnit
import kotlin.io.path.exists
import kotlin.io.path.isDirectory
import kotlin.io.path.name

interface Directory {
    val path: Path

    val locksPath: Path
        get() = path.resolve("locks")

    val pathString: String
        get() = path.toString()

    fun getPath(vararg pathChunks: String): Path {
        val path = Paths.get(pathString, *pathChunks)
        createSubDirectories(path)
        return path
    }

    fun getDirectory(vararg pathChunks: String): Directory {
        val path = getPath(*pathChunks)
        if (!path.exists()) createDirectories(path)
        if (!path.isDirectory()) path.toFile().mkdir()
        return object : Directory {
            override val path = path
        }
    }

    fun getFile(vararg pathChunks: String) = getPath(*pathChunks).toFile()
    fun exists(vararg pathChunks: String) = getFile(*pathChunks).exists()
    fun loadFileAsString(vararg pathChunks: String) = getFile(*pathChunks).readText()
    fun <T> save(data: T, vararg pathChunks: String) = save(data, GSON, *pathChunks)
    fun <T> save(data: T, gson: Gson = GSON, vararg pathChunks: String) = FileUtilities.save(getPath(*pathChunks), data, gson)
    fun <T> load(classType: Class<T>, vararg pathChunks: String) = FileUtilities.load(getPath(*pathChunks), classType, GSON)
    fun <T> load(classType: Class<T>, gson: Gson = GSON, vararg pathChunks: String) = FileUtilities.load(getPath(*pathChunks), classType, gson)

    fun copyUrlToFile(url: String, vararg pathChunks: String) = FileUtilities.copyURLtoFile(url, getFile(*pathChunks))
    fun writeToFile(text: String, vararg pathChunks: String) = writeFile(getPath(*pathChunks), text)
    fun clean() = deleteFilesOlderThan(5, TimeUnit.DAYS, path, true)
    fun cleanLocks() = deleteFilesOlderThan(5, TimeUnit.DAYS, locksPath, true)
    fun clear() = deleteFiles( {true}, path, true)
    fun getFilePathNoOverwrite(vararg pathChunks: String): Path {
        val path = getPath(*pathChunks)
        if (path.toFile().exists()) {
            val name = path.fileName.toString()
            val extension = name.substring(name.lastIndexOf("."))
            val nameNoExtension = name.substring(0, name.lastIndexOf("."))
            val newPath = path.parent.resolve("${nameNoExtension}_${System.currentTimeMillis()}$extension")
            return getFilePathNoOverwrite(newPath.toString())
        }
        if (!path.toFile().exists()) path.toFile().mkdir()
        return path
    }
    fun getDirectoryNoOverwrite(vararg pathChunks: String): Path {
        val path = getPath(*pathChunks)
        if (path.toFile().exists()) {
            val newPath = path.parent.resolve("${path.name}_${System.currentTimeMillis()}")
            return getDirectoryNoOverwrite(newPath.toString())
        }
        if (!path.toFile().exists()) path.toFile().mkdir()
        return path
    }

    companion object {
        @JvmStatic
        fun from(path: Path) = object : Directory {
            override val path: Path = path
        }

        @JvmStatic
        fun from(path: String, vararg chunks: String) = object : Directory {
            override val path: Path = Paths.get(path, *chunks)
        }

        @JvmStatic
        val TRiBot = from(FileUtilities.tribotDirectoryPath)
        val Temp = from(FileUtilities.tempDirectoryPath)
        val DP_TRiBot = TRiBot.getDirectory("detonate-productions")


    }

}