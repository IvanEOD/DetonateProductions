package com.detonateproductions.api

import com.google.gson.GsonBuilder
import kotlin.properties.Delegates

object Global {
    var GSON = GsonBuilder().create()

    private fun updateGitHub(value: Any, type: Int) {
        val directory = if (type == 0) value as Directory else GitHubDirectory
        val url = if (type == 1) value as String else GitHubUrl
        val token = if (type == 2) value as String else GitHubToken
        GitHub = GitHub(directory, url, token)
    }

    var GitHubDirectory by Delegates.observable(Directory.Temp) { _, _, newValue ->
        updateGitHub(newValue, 0)
    }

    var GitHubUrl by Delegates.observable("https://raw.githubusercontent.com/IvanEOD/public-resources/main/") { prop, oldValue, newValue ->
        updateGitHub(newValue, 1)
    }

    var GitHubToken by Delegates.observable("") { prop, oldValue, newValue ->
        updateGitHub(newValue, 2)
    }

    var GitHub = GitHub(Directory.Temp, GitHubUrl, GitHubToken)
        private set


}