@file:OptIn(org.jetbrains.compose.ExperimentalComposeLibrary::class)

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    id("org.jetbrains.compose")
    java
    `java-library`
    `maven-publish`
}

repositories {
    mavenCentral()
    maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    google()
}

group = "com.detonateproductions"
version = "1.0.0"

java.sourceCompatibility = JavaVersion.VERSION_11
java.targetCompatibility = JavaVersion.VERSION_11

dependencies {
    val composeBom = platform("androidx.compose:compose-bom:2022.12.00")

    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    api("com.google.code.gson:gson:2.10.1")
    api("commons-io:commons-io:2.11.0")
    api("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")

    api(composeBom)
    api(compose.desktop.currentOs)
    api(compose.material3)
    api(compose.materialIconsExtended)
    api(compose.foundation)
    api(compose.ui)
    api(compose.animation)

}

java {
    withSourcesJar()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "11"
        freeCompilerArgs += "-opt-in=kotlin.RequiresOptIn"
    }
}

tasks.jar {
    manifest {
        attributes(
            mapOf(
                "Implementation-Title"   to project.name,
                "Implementation-Version" to project.version
            )
        )
    }
}

publishing {
    repositories {
        maven {
            name = "GitHubPackages"
            url = uri("https://maven.pkg.github.com/IvanEOD/DetonateProductions")
            credentials {
                username = System.getenv("GITHUB_PACKAGES_USERID")
                password = System.getenv("GITHUB_PACKAGES_PUBLISH_TOKEN")
            }
        }
    }

    publications {
        register<MavenPublication>("gpr") {
            groupId = project.group.toString()
            artifactId = "lib"
            version = project.version.toString()
            from(components["java"])
        }
    }


}

//https://github-registry-files.githubusercontent.com/348776370/91308e00-9b2f-11ed-8e97-871f0e9e0da5?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20230123%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20230123T210001Z&X-Amz-Expires=300&X-Amz-Signature=d79cf91687bca8e0d2a2541982caa6776c99df06eb1c49f29ee6c8c6e5c7c846&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=348776370&response-content-disposition=filename%3Dlibrary-1.0.0.jar&response-content-type=application%2Foctet-stream